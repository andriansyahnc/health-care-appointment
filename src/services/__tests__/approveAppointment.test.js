const httpStatus = require("http-status");
const approveAppointment = require("../approveAppointment");
const appointmentRepository = require('../../repositories/appointment');

jest.mock('../../repositories/appointment');

describe('Approve Appointment', () => {

  test('I successfully approved an appointment', async () => {

    const mockAppointmentId = 1;
    const mockUser = {
      id: 2,
    };

    const mockAppointment = {
      id: mockAppointmentId,
      coachId: mockUser.id,
    };

    appointmentRepository.findById.mockReturnValue(mockAppointment);

    const result = await approveAppointment(mockAppointmentId, mockUser);
    expect(appointmentRepository.findById).toHaveBeenCalledWith(mockAppointmentId);
    expect(result).toEqual([true, "OK", httpStatus.OK]);
  })
});