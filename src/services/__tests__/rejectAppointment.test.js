const httpStatus = require("http-status");
const rejectAppointment = require("../rejectAppointment");
const appointmentRepository = require('../../repositories/appointment');

jest.mock('../../repositories/appointment');

describe('Reject Appointment', () => {

  test('I successfully rejected an appointment', async () => {

    const mockAppointmentId = 1;
    const mockUser = {
      id: 2,
    };

    const mockAppointment = {
      id: mockAppointmentId,
      coachId: mockUser.id,
    };

    appointmentRepository.findById.mockReturnValue(mockAppointment);

    const result = await rejectAppointment(mockAppointmentId, mockUser);
    expect(appointmentRepository.findById).toHaveBeenCalledWith(mockAppointmentId);
    expect(result).toEqual([true, "OK", httpStatus.OK]);
  })
});