
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Schedule extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: {
          allowNull: false,
        }
      })
    }
  }
  Schedule.init({
    userId: DataTypes.INTEGER,
    day: DataTypes.STRING,
    startTime: DataTypes.STRING,
    endTime: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Schedule',
    underscored: true,
    indexes: [
      {
        unique: true,
        fields: ['user_id', 'day'],
      },
    ],
  });
  return Schedule;
};