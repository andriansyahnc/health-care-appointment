const { ROLE } = require("../../constants/user");
const auth = require("../auth");
const isCoach = require('../isCoach');

// Mock the required dependencies
jest.mock("../auth");

// Mock the request and response objects
const req = {};
const res = { locals: {} };
const next = jest.fn();

// Define a test user
const testUser = {
  role: ROLE.COACH,
};

describe("isCoach middleware", () => {
  beforeEach(() => {
    // Clear any previous mock implementation calls
    auth.mockClear();
  });

  it("should call auth function with the request and response objects", () => {
    auth.mockReturnValue(testUser);

    isCoach(req, res, next);

    expect(auth).toHaveBeenCalledWith(req);
  });

  it("should set res.locals.user with the user role if the user is a coach", () => {
    auth.mockReturnValue(testUser);

    isCoach(req, res, next);

    expect(res.locals.user).toEqual(testUser);
  });

  it("should call next if the user is a coach", () => {
    auth.mockReturnValue(testUser);

    isCoach(req, res, next);

    expect(next).toHaveBeenCalled();
  });

  it("should call next with an error if the user is not a coach", () => {
    auth.mockReturnValue(ROLE.USER);

    isCoach(req, res, next);

    expect(next).toHaveBeenCalledWith(new Error('Forbidden'));
  });

  it("should call next with the thrown error if auth throws an error", () => {
    const testError = new Error("Authentication failed");
    auth.mockImplementation(() => {
      throw testError;
    });

    isCoach(req, res, next);

    expect(next).toHaveBeenCalledWith(testError);
  });
});
