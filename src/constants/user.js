const ROLE = {
  USER: 'user',
  COACH: 'coach',
}

module.exports = {
  ROLE,
}