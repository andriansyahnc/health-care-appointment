

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const basename = path.basename(__filename);
const config = require('../configs/config');

const db = {};

const sequelize = new Sequelize(config.database.name, config.database.user, config.database.pass, {
  host: config.database.host,
  dialect: 'mysql',
  // eslint-disable-next-line no-console
  logging: config.database.logging === "true" ? console.log : false,
});

fs
  .readdirSync(__dirname)
  .filter(file => (
      file.indexOf('.') !== 0 &&
      file !== basename &&
      file.slice(-3) === '.js' &&
      file.indexOf('.test.js') === -1
    ))
  .forEach(file => {
    // eslint-disable-next-line global-require, import/no-dynamic-require
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

if (process.env.NODE_ENV === 'development') {
  // sequelize.sync({ alter: true });
}

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
