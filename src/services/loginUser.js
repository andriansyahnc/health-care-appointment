const jwt = require('jsonwebtoken');
const userRepositories = require('../repositories/user')
const {isPasswordValid, signObject} = require("../utils/utils");
const config = require("../configs/config");

const loginUser = async (data) => {
  const loggedInUser = await userRepositories.findByEmail(data.email);

  if (!loggedInUser) {
    return [false, 'User not found']
  }

  const isPwdValid = isPasswordValid(data.password, loggedInUser.salt, loggedInUser.password);

  if (!isPwdValid) {
    return [false, 'User not authorized']
  }

  return [true, signObject({
    id: loggedInUser.id,
    email: loggedInUser.email,
    role: loggedInUser.role,
    tz: loggedInUser.tz,
  })];

}

module.exports = loginUser;