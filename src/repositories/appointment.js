const {Op} = require("sequelize");
const Model = require('../models');
const { APPOINTMENT_STATE: { PENDING, APPROVED, REJECTED, RESCHEDULED } } = require('../constants/appointment');

const bookAppointment = (appointment, userId) => Model.Appointment.create({
    ...appointment,
    state: PENDING,
    userId,
  })

const findById = (id) => Model.Appointment.findOne({
  where: {
    id,
  }
});

const findAll = (where) => Model.Appointment.findAll({
  where,
  order: [['id', 'DESC']],
});

const updateAppointmentById = (id, data) => Model.Appointment.update(data, {
  where: {
    id,
  },
  individualHooks: true
});

const approveAppointment = (id, userId) => updateAppointmentById(id, { updatedById: userId, state: APPROVED });
const rejectAppointment = (id, userId) => updateAppointmentById(id, { updatedById: userId, state: REJECTED });
const rescheduleAppointment = (id, userId, startTime, endTime) => updateAppointmentById(id, { updatedById: userId, state: RESCHEDULED, startTime, endTime });

const findApprovedAppointmentBetweenProposedTime = (proposed, userId) => Model.Appointment.count({
    where: {
      [Op.or]: [
        {
          [Op.and]: [
            {
              [Op.or]: [
                {
                  startTime: {
                    [Op.between]: [proposed.startTime, proposed.endTime],
                  },
                },
                {
                  endTime: {
                    [Op.between]: [proposed.startTime, proposed.endTime],
                  },
                }
              ]
            },
            { state: APPROVED },
            { coachId: proposed.coachId },
          ]
        },
        {
          [Op.and]: [
            {
              [Op.or]: [
                {
                  startTime: {
                    [Op.between]: [proposed.startTime, proposed.endTime],
                  },
                },
                {
                  endTime: {
                    [Op.between]: [proposed.startTime, proposed.endTime],
                  },
                }
              ]
            },
            { state: PENDING },
            { userId },
          ]
        }
      ]
    }
  });

module.exports = {
  bookAppointment,
  approveAppointment,
  findById,
  rejectAppointment,
  rescheduleAppointment,
  findAll,
  findApprovedAppointmentBetweenProposedTime,
}