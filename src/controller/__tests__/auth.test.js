const request = require("supertest");
const httpStatus = require("http-status");
const app = require("../../app");
const {newUserInput, newUserResponse, newUserInput2} = require("../../../test/data/user");
const globalDatabase = require("../../../test/function/makeGlobalDatabase");
const userRepository = require("../../repositories/user");

describe('Auth', () => {

  afterAll(async () => {
    await globalDatabase.closeGlobalDatabase();
    // eslint-disable-next-line no-promise-executor-return
    await new Promise((resolve) => setTimeout(() => resolve(), 1000));
  })

  describe('Register', () => {

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();
    });

    test('Successfully register coach', async () => {
      const body = newUserInput;
      const response = await request(app)
        .post('/auth/coach/register')
        .send(body);

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body).toEqual({
        success: true,
        message: 'OK',
        data: newUserResponse,
      });
    });

    test('Successfully register user', async () => {
      const body = newUserInput;
      const response = await request(app)
        .post('/auth/user/register')
        .send(body);

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body).toEqual({
        success: true,
        message: 'OK',
        data: newUserResponse,
      });
    });

  });

  describe('Login', () => {

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();

      await userRepository.insertAsUser(newUserInput2);
      await userRepository.insertAsCoach(newUserInput);
    });

    test('Successfully login user', async () => {
      const body = {
        email: newUserInput.email,
        password: newUserInput.password,
      }

      const response = await request(app)
        .post('/auth/login')
        .send(body);

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body).toEqual({
        success: true,
        message: 'OK',
        data: expect.anything(),
      });
    })
  });
});