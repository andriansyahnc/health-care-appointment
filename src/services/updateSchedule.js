const scheduleRepository = require('../repositories/schedule');

const updateSchedule = async (schedule) => {
  const scheduleData = await scheduleRepository.upsertMany(schedule);
  return scheduleData.map(scheduleResult => ({
    id: scheduleResult.id,
    day: scheduleResult.day,
    startTime: scheduleResult.startTime,
    endTime: scheduleResult.endTime,
  }));
};

module.exports = updateSchedule;