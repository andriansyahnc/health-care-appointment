const dotenv = require("dotenv");

dotenv.config();

module.exports = {
  database: {
    host: process.env.MYSQL_HOST,
    name: process.env.MYSQL_DATABASE,
    user: process.env.MYSQL_USER,
    pass: process.env.MYSQL_PASSWORD,
    logging: process.env.DB_LOGGING,
  },
  key: {
    jwt: process.env.JWT_KEY,
  }
}