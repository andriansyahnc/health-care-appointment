const router = require('express').Router();
const controller = require("../controller/schedule");
const isCoachMiddleware = require('../middlewares/isCoach');

router.patch('/:id/availability', isCoachMiddleware, controller.updateSchedule);

module.exports = router;