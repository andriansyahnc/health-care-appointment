const Model = require('../models')

const getLatestStateByAppointmentId = (appointmentId) => Model.AppointmentState.findOne({
    where: {
      appointmentId,
    },
    order: [['id', 'DESC']],
  })

module.exports = {
  getLatestStateByAppointmentId,
}