const httpStatus = require("http-status");
const dayjs = require("dayjs");
const utc = require("dayjs/plugin/utc");
const timezone = require("dayjs/plugin/timezone");
const appointmentRepository = require('../repositories/appointment');

dayjs.extend(utc);
dayjs.extend(timezone);

const rescheduleAppointment = async (appointmentId, user, startTime, endTime) => {
  const appointment = await appointmentRepository.findById(appointmentId);
  if (!appointment) {
    return [false, 'Appointment not found', httpStatus.NOT_FOUND];
  }

  if (appointment.coachId !== user.id) {
    return [false, 'Forbidden', httpStatus.FORBIDDEN];
  }

  const proposedStartTime = dayjs.tz(startTime, user.tz);
  const proposedEndTime = dayjs.tz(endTime, user.tz);

  await appointmentRepository.rescheduleAppointment(appointmentId, user.id, proposedStartTime, proposedEndTime);
  return [true, 'OK', httpStatus.OK];
}

module.exports = rescheduleAppointment;