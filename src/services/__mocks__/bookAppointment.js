const httpStatus = require("http-status");

const mockRequestBody = {
  startTime: "1 January 2023 00:30",
  endTime: "1 January 2023 01:30",
  coachId: 1,
};

const bookAppointmentMock = jest.fn().mockReturnValue([
  true, {
    ...mockRequestBody
  },
  httpStatus.OK]
);

module.exports = bookAppointmentMock;