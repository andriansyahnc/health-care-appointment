const schedule1 = [
  {
    day: 'Monday',
    startTime: '09:00',
    endTime: '17:00'
  },
  {
    day: 'Tuesday',
    startTime: '09:00',
    endTime: '17:00'
  },
  {
    day: 'Wednesday',
    startTime: '09:00',
    endTime: '17:00'
  },
  {
    day: 'Thursday',
    startTime: '09:00',
    endTime: '17:00'
  },
  {
    day: 'Friday',
    startTime: '09:00',
    endTime: '17:00'
  },
  {
    day: 'Saturday',
    startTime: null,
    endTime: null
  },
  {
    day: 'Sunday',
    startTime: null,
    endTime: null
  }
];

module.exports = {
  schedule1,
}