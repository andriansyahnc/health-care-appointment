const request = require('supertest');
const app = require('../../app');
const globalDatabase = require("../../../test/function/makeGlobalDatabase");

describe('Health', () => {

    beforeEach(async () => {
        await globalDatabase.syncGlobalDatabase();
    });

    afterAll(async () => {
        await globalDatabase.closeGlobalDatabase();
        // eslint-disable-next-line no-promise-executor-return
        await new Promise((resolve) => setTimeout(() => resolve(), 1000));
    })

    test('Successfully get health response', async () => {
        const response = await request(app)
            .get('/health');

        expect(response.body).toEqual({
            success: true,
            message: 'OK',
        });
    })
})