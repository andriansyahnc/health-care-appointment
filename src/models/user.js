
const {
  Model
} = require('sequelize');
const {ROLE} = require("../constants/user");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Schedule);
      this.hasMany(models.Appointment, { foreignKey: 'userId', as: 'appointments' });
      this.hasMany(models.Appointment, { foreignKey: 'coachId', as: 'coachAppointments' });
    }
  }
  User.init({
    email: {
      type: DataTypes.STRING(100),
      unique: true,
    },
    password: DataTypes.STRING,
    tz: DataTypes.STRING(100),
    name: DataTypes.STRING(100),
    photo: DataTypes.STRING,
    role: {
      type: DataTypes.ENUM,
      values: [ROLE.USER, ROLE.COACH]
    },
    salt: DataTypes.STRING,
    createdById: DataTypes.INTEGER.UNSIGNED,
    updatedById: DataTypes.INTEGER.UNSIGNED,
    deletedById: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'User',
    underscored: true,
    paranoid: true,
    timestamps: true,
  });
  return User;
};