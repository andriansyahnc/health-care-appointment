const Model = require('../../src/models/index')

const syncGlobalDatabase = async () => {
  await Model.sequelize.sync({ force: true });
}

const closeGlobalDatabase = async () => {
  await Model.sequelize.close();
}

module.exports = {
  syncGlobalDatabase,
  closeGlobalDatabase,
};