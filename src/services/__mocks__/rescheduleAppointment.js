const httpStatus = require("http-status");

const rescheduleAppointment = jest.fn().mockReturnValue([
  true, 'OK', httpStatus.OK]
);

module.exports = rescheduleAppointment;
