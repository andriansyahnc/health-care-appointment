const router = require('express').Router();
const controller = require("../controller/auth");

router.post('/coach/register', controller.coachRegister);
router.post('/user/register', controller.userRegister);

router.post('/login', controller.userLogin);

module.exports = router;