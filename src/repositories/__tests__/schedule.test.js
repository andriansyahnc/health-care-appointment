const globalDatabase = require("../../../test/function/makeGlobalDatabase");
const userRepository = require("../user");
const scheduleRepository = require("../schedule");
const {newUserInput2, newUserInput} = require("../../../test/data/user");
const {schedule1} = require("../../../test/data/coachSchedule");
const db = require('../../models/index')

describe('Schedule Repository', () => {

  afterAll(async () => {
    await globalDatabase.closeGlobalDatabase();
    // eslint-disable-next-line no-promise-executor-return
    await new Promise((resolve) => setTimeout(() => resolve(), 1000));
  });

  describe('Insert', () => {

    let coach;

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();

      await userRepository.insertAsUser(newUserInput2);
      coach = await userRepository.insertAsCoach(newUserInput);
    });

    test('successfully insert the schedule', async () => {

      const scheduleInputs = schedule1.map(schedule => ({
          ...schedule,
          userId: coach.id
        }))

      const schedules = await scheduleRepository.upsertMany(scheduleInputs);

      expect(schedules.length).toBe(schedule1.length);
      schedules.forEach(schedule => {
        expect(schedule.id).not.toBeNull();
        expect(schedule.id).not.toBeUndefined();
      })
    });
  })

  describe('Update', () => {

    let coach;
    let existingSchedule;

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();

      await userRepository.insertAsUser(newUserInput2);
      coach = await userRepository.insertAsCoach(newUserInput);

      const t = await db.sequelize.transaction();
      const scheduleInputs = schedule1.map(schedule => ({
          ...schedule,
          userId: coach.id
        }))
      existingSchedule = await scheduleRepository.upsertMany(scheduleInputs, t);
      await t.commit();
    });

    test('successfully update the schedule', async () => {
      const scheduleInput2 = existingSchedule.map(schedule => ({
        endTime: schedule.endTime,
        startTime: '07:00',
        day: schedule.day,
        userId: schedule.userId,
        id: schedule.id,
      }));

      const t = await db.sequelize.transaction();
      const schedules = await scheduleRepository.upsertMany(scheduleInput2, t);
      await t.commit();

      expect(schedules.length).toBe(schedule1.length);
      schedules.forEach(schedule => {
        expect(schedule.id).not.toBeNull();
        expect(schedule.id).not.toBeUndefined();
        expect(schedule.startTime).toBe('07:00');
      })
    });
  })
})