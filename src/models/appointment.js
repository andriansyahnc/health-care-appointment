
const {
  Model
} = require('sequelize');
const { APPOINTMENT_STATE: { PENDING, APPROVED, CANCELLED, REJECTED, RESCHEDULED, TERMINATED} } = require('../constants/appointment');
const {ROLE} = require("../constants/user");

module.exports = (sequelize, DataTypes) => {
  class Appointment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: 'coach_id',
        as: 'coach',
        scope: {
          role: ROLE.COACH,
        },
      });

      this.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'user',
        scope: {
          role: 'user',
        },
      });

      this.hasMany(models.AppointmentState);

      this.belongsTo(models.User, {
        foreignKey: 'updated_by_id',
        as: 'updated_by',
      });
    }

    static async createAppointmentState(appointment, userId) {
      const {AppointmentState} = sequelize.models;
      await AppointmentState.create({
        appointmentId: appointment.id,
        state: appointment.state,
        createdById: userId,
        detail: appointment.detail,
        startTime: appointment.startTime,
        endTime: appointment.endTime,
      });
    }
  }
  Appointment.init({
    userId: DataTypes.INTEGER,
    coachId: DataTypes.INTEGER,
    startTime: DataTypes.DATE,
    endTime: DataTypes.DATE,
    state: {
      type: DataTypes.ENUM,
      values: [PENDING, APPROVED, RESCHEDULED, REJECTED, CANCELLED, TERMINATED],
    },
    detail: DataTypes.STRING,
    calendarLink: DataTypes.STRING,
    updatedById: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Appointment',
    underscored: true,
  });

  Appointment.afterCreate(async (appointment) => {
    await Appointment.createAppointmentState(appointment, appointment.userId);
  });

  Appointment.afterUpdate(async (appointment) => {
    await Appointment.createAppointmentState(appointment, appointment.updatedById);
  });

  return Appointment;
};