const registerNewCoach = require('../registerNewCoach');
const {newUserInput, newUserResponse} = require("../../../test/data/user");
const globalDatabase = require("../../../test/function/makeGlobalDatabase");

describe('Register New Coach', () => {

  beforeEach(async () => {
    await globalDatabase.syncGlobalDatabase();
  });

  afterAll(async () => {
    await globalDatabase.closeGlobalDatabase();
    // eslint-disable-next-line no-promise-executor-return
    await new Promise((resolve) => setTimeout(() => resolve(), 1000));
  })

  test('Succeed', async () => {
    const input = newUserInput;
    const newCoach = await registerNewCoach(input);
    expect(newCoach).toEqual(newUserResponse)
  })
})