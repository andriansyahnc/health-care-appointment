const globalDatabase = require("../../../test/function/makeGlobalDatabase");
const updateSchedule = require("../updateSchedule");
const userRepository = require("../../repositories/user");
const {newUserInput2, newUserInput} = require("../../../test/data/user");
const {schedule1} = require("../../../test/data/coachSchedule");

describe('Update new service', () => {

  let coach;

  beforeEach(async () => {
    await globalDatabase.syncGlobalDatabase();

    coach = await userRepository.insertAsUser(newUserInput2);
    await userRepository.insertAsCoach(newUserInput);
  });

  afterAll(async () => {
    await globalDatabase.closeGlobalDatabase();
    // eslint-disable-next-line no-promise-executor-return
    await new Promise((resolve) => setTimeout(() => resolve(), 1000));
  })

  test('success', async () => {

    const scheduleInput = schedule1.map(schedule => ({
      ...schedule,
      userId: coach.id,
    }));

    const schedules = await updateSchedule(scheduleInput);

    expect(schedules.length).toBe(schedule1.length);
  })
})