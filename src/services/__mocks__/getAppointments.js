const httpStatus = require("http-status");

const getAppointments = jest.fn().mockReturnValue([
  true, [], httpStatus.OK]
);

module.exports = getAppointments;
