const httpStatus = require("http-status");
const registerNewCoach = require('../services/registerNewCoach');
const registerNewUser = require('../services/registerNewUser');
const loginUser = require('../services/loginUser');

const coachRegister = async (req, res, next) => {
  try {
    const {body} = req;
    const data = await registerNewCoach(body);
    return res.status(httpStatus.OK).send({
      success: true,
      message: 'OK',
      data,
    });
  } catch (e) {
    console.error(e);
    return next(e);
  }
}

const userRegister = async (req, res, next) => {
  try {
    const {body} = req;
    const data = await registerNewUser(body);
    return res.status(httpStatus.OK).send({
      success: true,
      message: 'OK',
      data,
    });
  } catch (e) {
    console.error(e);
    return next(e);
  }
}

const userLogin = async (req, res, next) => {
  try {
    const {body} = req;
    const [data, message] = await loginUser(body);
    if (!data) {
      return res.status(httpStatus.UNAUTHORIZED).send({
        success: false,
        message: 'Not OK',
      });
    }
    return res.status(httpStatus.OK).send({
      success: true,
      message: 'OK',
      data: {
        token: message
      },
    });
  } catch (e) {
    console.error(e);
    return next(e);
  }
}

module.exports = {
  coachRegister,
  userRegister,
  userLogin,
}