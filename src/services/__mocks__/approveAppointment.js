const httpStatus = require("http-status");

const approveAppointmentMock = jest.fn().mockReturnValue([
  true, 'OK', httpStatus.OK]
);

module.exports = approveAppointmentMock;
