const httpStatus = require("http-status");
const getAppointments = require("../getAppointments");
const appointmentRepository = require('../../repositories/appointment');
const {ROLE} = require("../../constants/user");

jest.mock('../../repositories/appointment');

describe('Get Appointment', () => {

  test('I successfully find all of my appointment(s)', async () => {

    const mockAppointmentId = 1;
    const mockUser = {
      id: 2,
      role: ROLE.COACH
    };

    const mockAppointment = {
      id: mockAppointmentId,
      coachId: mockUser.id,
      userId: 3
    };

    appointmentRepository.findAll.mockReturnValue([mockAppointment]);

    const result = await getAppointments(mockUser, null);
    expect(appointmentRepository.findAll).toHaveBeenCalledWith({
      coachId: mockUser.id,
    });
    expect(result).toEqual([true, [mockAppointment], httpStatus.OK]);
  })
});