const Model = require('../models')

const upsert = (schedule, tx) => Model.Schedule.upsert(schedule, tx)

const upsertMany = async (schedules, tx) => {
  const res = [];
  for (let i = 0; i < schedules.length; i += 1) {
    const [newSchedule] = await upsert(schedules[i], tx);
    res.push(newSchedule);
  }
  return res;
}

const getScheduleByCoachId = (coachId) => Model.Schedule.findALl({
    coachId,
  })

module.exports = {
  upsertMany,
  getScheduleByCoachId,
}