const httpStatus = require("http-status");

const getHealth = async (req, res, next) => {
  try {
    res.status(httpStatus.OK).send({
      success: true,
      message: 'OK',
    })
  } catch (e) {
    next(e);
  }
};

module.exports = {
  getHealth,
}