const userRepositories = require('../repositories/user')

const registerNewCoach = async (data) => {
  const newCoach = await userRepositories.insertAsUser(data);
  return {
    email: newCoach.email,
    name: newCoach.name,
    tz: newCoach.tz,
  }
}

module.exports = registerNewCoach;