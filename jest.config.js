module.exports = {
    bail: 1,
    clearMocks: true,
    coverageProvider: "v8",
    setupFiles: ["<rootDir>/test/setup-tests.js"],
    testEnvironment: "node",
    testMatch: [
        "**/__tests__/**/*.[jt]s?(x)",
        "**/?(*.)+(spec|test).[tj]s?(x)",
    ],
    transformIgnorePatterns: [
        "<rootDir>/node_modules/",
    ],
};
