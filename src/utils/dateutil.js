const dayjs = require("dayjs");
const utc = require("dayjs/plugin/utc");
const timezone = require("dayjs/plugin/timezone");
const isBetween = require("dayjs/plugin/isBetween");

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(isBetween);

const isProposedTimeValid = (coachSchedule, coachTz, proposedTime, proposedTz) => {

  const proposedStartTime = dayjs.tz(proposedTime.startTime, proposedTz);
  const proposedEndTime = dayjs.tz(proposedTime.endTime, proposedTz);

  const proposedStartTimeInCoachTz = proposedStartTime.clone().tz(coachTz);
  const dayInProposedStartTimeCoachTz = proposedStartTimeInCoachTz.format("dddd");
  const dateInProposedStartTimeCoachTz = proposedStartTimeInCoachTz.format(
    "YYYY-MM-DD"
  );

  const proposedEndTimeInCoachTz = proposedEndTime.clone().tz(coachTz);
  const dayInProposedEndTimeCoachTz = proposedEndTimeInCoachTz.format("dddd");

  const nowSchedule = coachSchedule.schedule.find(schedule => schedule.day === dayInProposedStartTimeCoachTz);
  if (!nowSchedule.startTime || !nowSchedule.endTime) {
    return false;
  }
  const availabilities = {
    startTime: dayjs.tz(`${dateInProposedStartTimeCoachTz} ${nowSchedule.startTime}`, coachTz),
    endTime: dayjs.tz(`${dateInProposedStartTimeCoachTz} ${nowSchedule.endTime}`, coachTz),
  };

  if (dayInProposedEndTimeCoachTz !== dayInProposedStartTimeCoachTz) {
    return false;
  }

  return (
    proposedStartTime.isSame(availabilities.startTime) ||
    proposedStartTime.isBetween(availabilities.startTime, availabilities.endTime)
  ) && (
    proposedEndTime.isBetween(availabilities.startTime, availabilities.endTime) ||
    proposedEndTime.isSame(availabilities.endTime)
  )

}

module.exports = {
  isProposedTimeValid,
};