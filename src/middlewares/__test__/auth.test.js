const { verifySignedObject } = require("../../utils/utils");
const auth = require("../auth");

// Mock the required dependencies
jest.mock("../../utils/utils");

describe("auth function", () => {
  let req;

  beforeEach(() => {
    req = {
      header: jest.fn(),
      params: {},
    };
    verifySignedObject.mockClear();
  });

  it("should throw an error if bearer token is not valid", () => {
    req.header.mockReturnValue("InvalidToken");

    expect(() => {
      auth(req);
    }).toThrowError("Bearer token not valid");
  });

  it("should throw an error if verification of signed object fails", () => {
    req.header.mockReturnValue("Bearer validToken");
    verifySignedObject.mockReturnValue(null);

    expect(() => {
      auth(req);
    }).toThrowError("Unauthorized");
  });

  it("should return the verified object if all checks pass", () => {
    const verifiedObject = {
      id: 123,
    };
    req.header.mockReturnValue("Bearer validToken");
    verifySignedObject.mockReturnValue(verifiedObject);
    req.params.id = 123;

    const result = auth(req);

    expect(result).toEqual(verifiedObject);
  });
});
