const httpStatus = require("http-status");
const appointmentRepository = require('../repositories/appointment');

const approveAppointment = async (appointmentId, user) => {
  const appointment = await appointmentRepository.findById(appointmentId);
  if (!appointment) {
    return [false, 'Appointment not found', httpStatus.NOT_FOUND];
  }

  if (appointment.coachId !== user.id) {
    return [false, 'Forbidden', httpStatus.FORBIDDEN];
  }
  await appointmentRepository.approveAppointment(appointmentId, user.id);
  return [true, 'OK', httpStatus.OK];
}

module.exports = approveAppointment;