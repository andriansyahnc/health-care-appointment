const registerNewUser = require('../registerNewUser');
const {newUserInput, newUserResponse} = require("../../../test/data/user");
const globalDatabase = require("../../../test/function/makeGlobalDatabase");

describe('Register New Coach', () => {

  beforeEach(async () => {
    await globalDatabase.syncGlobalDatabase();
  });

  afterAll(async () => {
    await globalDatabase.closeGlobalDatabase();
    // eslint-disable-next-line no-promise-executor-return
    await new Promise((resolve) => setTimeout(() => resolve(), 1000));
  })

  test('Succeed', async () => {
    const input = newUserInput;
    const newUser = await registerNewUser(input);
    expect(newUser).toEqual(newUserResponse)
  })
})