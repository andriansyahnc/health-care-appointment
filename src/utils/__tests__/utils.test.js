const {getPassword, isPasswordValid, signObject, verifySignedObject} = require("../utils");
const {newUserInput} = require("../../../test/data/user");
const {ROLE} = require("../../constants/user");

describe('Utils', () => {
  test('getPassword and isValidPassword', () => {
    const password = "myRandomPassword";
    const [hash, salt] = getPassword(password);
    expect(isPasswordValid(password, salt, hash)).toBe(true);
  })

  test('sign and verify', () => {
    const obj = {
      id: 1,
      email: newUserInput.email,
      role: ROLE.COACH,
      tz: newUserInput.tz,
    }
    const signedObject1 = signObject(obj);
    const newObj = verifySignedObject(signedObject1);

    expect(newObj).toMatchObject(obj);
  })
})