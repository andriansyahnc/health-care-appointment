const dateutil = require('../dateutil');
const {schedule1} = require("../../../test/data/coachSchedule");

describe('dateutil', () => {
  describe('isProposedTimeValid', () => {
    test.each([
      [
        'proposed time is between coach availability time',
        {
          schedule: schedule1,
        },
        "Europe/Kiev",
        {
          startTime: "2023-06-01 18:00",
          endTime: "2023-06-01 19:00",
        },
        "Asia/Jakarta",
        true
      ],
      [
        'proposed time is outside coach availability time',
        {
          schedule: schedule1,
        },
        "Europe/Kiev",
        {
          startTime: "2023-06-01 21:00",
          endTime: "2023-06-01 22:00",
        },
        "Asia/Jakarta",
        false
      ],
      [
        'part of proposed end time is outside coach availability time',
        {
          schedule: schedule1,
        },
        "Europe/Kiev",
        {
          startTime: "2023-06-01 20:30",
          endTime: "2023-06-01 21:30",
        },
        "Asia/Jakarta",
        false
      ],
      [
        'part of proposed start time is outside coach availability time',
        {
          schedule: schedule1,
        },
        "Europe/Kiev",
        {
          startTime: "2023-06-01 05:30",
          endTime: "2023-06-01 06:30",
        },
        "Asia/Jakarta",
        false
      ],
      [
        'van - proposed time is between coach availability time',
        {
          schedule: schedule1,
        },
        "America/Vancouver",
        {
          startTime: "2023-06-02 00:00",
          endTime: "2023-06-02 01:00",
        },
        "Asia/Jakarta",
        true
      ],
      [
        'van - proposed time is outside coach availability time',
        {
          schedule: schedule1,
        },
        "America/Vancouver",
        {
          startTime: "2023-06-01 19:00",
          endTime: "2023-06-01 20:00",
        },
        "Asia/Jakarta",
        false
      ],
      [
        'van - part of proposed end time is outside coach availability time',
        {
          schedule: schedule1,
        },
        "America/Vancouver",
        {
          startTime: "2023-06-02 22:30",
          endTime: "2023-06-02 23:30",
        },
        "Asia/Jakarta",
        false
      ],
      [
        'van - part of proposed start time is outside coach availability time',
        {
          schedule: schedule1,
        },
        "America/Vancouver",
        {
          startTime: "2023-06-01 19:30",
          endTime: "2023-06-02 20:30",
        },
        "Asia/Jakarta",
        false
      ]
    ])('isBetween - %s', (label, coachtime, coachTz, proposedtime, proposedTz, expected) => {
      const isValid = dateutil.isProposedTimeValid(coachtime, coachTz, proposedtime, proposedTz);

      expect(isValid).toBe(expected)
    })
  })
})