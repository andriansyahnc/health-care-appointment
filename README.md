# Healthcare - TS

## Overview

This repository is to manage appointment of a user.

## Tech stack

1. NodeJs
2. Volta (for managing node version) (https://docs.volta.sh/guide/getting-started)
3. Jest + Supertest (for testing purpose)

## How to install

1. run `cp .env.example .env`
2. change configuration accordingly
3. run `npm install`

## How to run the project

1. run `npm run start`

## Documentation

https://wild-fan-514.notion.site/Fita-test-analisys-b68dd09942824d4788a0f69f74a31f8d

## Postman collection

https://www.postman.com/lunar-comet-872695/workspace/hca/collection/120090-b28de1fb-3fe3-4ad4-9d9b-0d51c3f04c4e?action=share&creator=120090

## Some other notes.

1. I just realized that I don't have migration in the half of the code, so I use sync instead.
