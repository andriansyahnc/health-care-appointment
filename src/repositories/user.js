const Model = require('../models')
const {getPassword} = require("../utils/utils");
const {ROLE} = require("../constants/user");

const insert = (user, role) => {
  const [password, salt] = getPassword(user.password);
  return Model.User.create({
    ...user,
    password,
    salt,
    role,
  })
}

const insertAsCoach = (user) => insert(user, ROLE.COACH);
const insertAsUser = (user) => insert(user, ROLE.USER);

const findByEmail = (email) => Model.User.findOne({
    where: {
      email
    }
  })

const getCoachDataById = (id) => Model.User.findOne({
  where: {
    id,
    role: ROLE.COACH,
  },
  include: {
    model: Model.Schedule,
  },
})

module.exports = {
  insertAsCoach,
  insertAsUser,
  findByEmail,
  getCoachDataById,
}
