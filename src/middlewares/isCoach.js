const {ROLE} = require("../constants/user");
const auth = require("./auth");

const isCoach = (req, res, next) => {
  try {
    const user = auth(req);
    if (user.role !== ROLE.COACH) {
      const error = Error('Forbidden');
      return next(error);
    }
    res.locals.user = user;
    return next();
  } catch (e) {
    return next(e);
  }
}

module.exports = isCoach;