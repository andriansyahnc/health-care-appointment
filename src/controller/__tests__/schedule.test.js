const request = require('supertest');
const httpStatus = require("http-status");
const app = require('../../app');
const globalDatabase = require("../../../test/function/makeGlobalDatabase");
const {newUserInput, newUserInput2} = require("../../../test/data/user");
const {signObject} = require("../../utils/utils");
const {schedule1} = require("../../../test/data/coachSchedule");
const userRepository = require("../../repositories/user");

describe('Profile', () => {

    let coach;

    beforeEach(async () => {
        await globalDatabase.syncGlobalDatabase();

        await userRepository.insertAsUser(newUserInput2);
        coach = await userRepository.insertAsCoach(newUserInput);
    });

    afterAll(async () => {
        await globalDatabase.closeGlobalDatabase();
        // eslint-disable-next-line no-promise-executor-return
        await new Promise((resolve) => setTimeout(() => resolve(), 1000));
    })

    test('Successfully patch profile', async () => {

        const token = signObject({
            id: coach.id,
            email: coach.email,
            role: coach.role,
            tz: coach.tz,
        });

        const response = await request(app)
          .patch(`/user/${coach.id}/availability`)
          .set('Authorization', `Bearer ${token}`)
          .send({
              data: schedule1,
          });

        // expect(response.status).toEqual(httpStatus.OK);
        expect(response.body).toMatchObject({
            success: true,
            message: 'OK',
            data: expect.anything(),
        });

        expect(response.body.data.length).toBe(schedule1.length);
    })
})