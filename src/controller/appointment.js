const bookAppointmentService = require('../services/bookAppointment');
const approveAppointmentService = require('../services/approveAppointment');
const rejectAppointmentService = require('../services/rejectAppointment');
const rescheduleAppointmentService = require('../services/rescheduleAppointment');
const getAppointmentsService = require('../services/getAppointments');

const bookAppointment = async (req, res, next) => {
  try {
    const [success, newAppointment, statusHttp] = await bookAppointmentService(req.body, res.locals.user);
    if (!success) {
      return res.status(statusHttp).send({
        success,
        message: 'NOT OK',
        errorMessage: newAppointment
      })
    }
    return res.status(statusHttp).send({
      success,
      message: 'OK',
      data: {
        newAppointment
      }
    });
  } catch (e) {
    console.error(e);
    return next(e);
  }
}

const approveAppointment = async (req, res, next) => {
  try {
    const [success, newAppointment, statusHttp] = await approveAppointmentService(req.params.id, res.locals.user);
    if (!success) {
      return res.status(statusHttp).send({
        success,
        message: 'NOT OK',
        errorMessage: newAppointment
      })
    }
    return res.status(statusHttp).send({
      success,
      message: 'OK',
      data: newAppointment,
    });
  } catch (e) {
    console.error(e);
    return next(e);
  }
}

const rejectAppointment = async (req, res, next) => {
  try {
    const [success, newAppointment, statusHttp] = await rejectAppointmentService(req.params.id, res.locals.user);
    if (!success) {
      return res.status(statusHttp).send({
        success,
        message: 'NOT OK',
        errorMessage: newAppointment
      })
    }
    return res.status(statusHttp).send({
      success,
      message: 'OK',
      data: newAppointment
    });
  } catch (e) {
    console.error(e);
    return next(e);
  }
}

const rescheduleAppointment = async (req, res, next) => {
  try {
    const [success, newAppointment, statusHttp] = await rescheduleAppointmentService(req.params.id, res.locals.user, req.body.startTime, req.body.endTime);
    if (!success) {
      return res.status(statusHttp).send({
        success,
        message: 'NOT OK',
        errorMessage: newAppointment
      })
    }
    return res.status(statusHttp).send({
      success,
      message: 'OK',
      data: {
        newAppointment
      }
    });
  } catch (e) {
    console.error(e);
    return next(e);
  }
}

const getAppointments = async (req, res, next) => {
  try {
    const [success, newAppointment, statusHttp] = await getAppointmentsService(res.locals.user);
    if (!success) {
      return res.status(statusHttp).send({
        success,
        message: 'NOT OK',
        errorMessage: newAppointment
      })
    }
    return res.status(statusHttp).send({
      success,
      message: 'OK',
      data: {
        newAppointment
      }
    });
  } catch (e) {
    console.error(e);
    return next(e);
  }
}

module.exports = {
  bookAppointment,
  approveAppointment,
  rejectAppointment,
  rescheduleAppointment,
  getAppointments,
}