const globalDatabase = require("../../../test/function/makeGlobalDatabase");
const userRepository = require("../user");
const {newUserInput2, newUserInput} = require("../../../test/data/user");
const db = require("../../models");
const {schedule1} = require("../../../test/data/coachSchedule");
const scheduleRepository = require("../schedule");
const appointmentRepository = require("../appointment");
const appointmentStateRepository = require("../appointmentstate");
const {APPOINTMENT_STATE} = require("../../constants/appointment");

describe('Appointment', () => {
  afterAll(async () => {
    await globalDatabase.closeGlobalDatabase();
    // eslint-disable-next-line no-promise-executor-return
    await new Promise((resolve) => setTimeout(() => resolve(), 1000));
  })

  describe('Booking Appointment', () => {

    let coach;
    let user;

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();

      user = await userRepository.insertAsUser(newUserInput2);
      coach = await userRepository.insertAsCoach(newUserInput);
    });

    test('succeed booked an appointment', async () => {
      const appointmentInput = {
        coachId: coach.id,
      }

      const newBooking = await appointmentRepository.bookAppointment(appointmentInput, user.id);
      const appointmentState = await appointmentStateRepository.getLatestStateByAppointmentId(newBooking.id);

      expect(newBooking.state).toBe(APPOINTMENT_STATE.PENDING);
      expect(newBooking.id).not.toBeNull();
      expect(newBooking.id).not.toBeUndefined();

      expect(appointmentState.id).not.toBeNull();
      expect(appointmentState.id).not.toBeUndefined();
      expect(appointmentState.state).toBe(APPOINTMENT_STATE.PENDING);
    })

  })

  describe('State Change', () => {

    let coach;
    let user;
    let existingBooking;

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();

      user = await userRepository.insertAsUser(newUserInput2);
      coach = await userRepository.insertAsCoach(newUserInput);
      const appointmentInput = {
        coachId: coach.id,
      }

      existingBooking = await appointmentRepository.bookAppointment(appointmentInput, user.id);
    });

    test('APPROVED a appointment', async () => {
      await appointmentRepository.approveAppointment(existingBooking.id, user.id);

      const appointmentState = await appointmentStateRepository.getLatestStateByAppointmentId(existingBooking.id);

      expect(appointmentState.state).toBe(APPOINTMENT_STATE.APPROVED);
    })

    test('REJECTED a appointment', async () => {
      await appointmentRepository.rejectAppointment(existingBooking.id, user.id);

      const appointmentState = await appointmentStateRepository.getLatestStateByAppointmentId(existingBooking.id);

      expect(appointmentState.state).toBe(APPOINTMENT_STATE.REJECTED);
    })

    test('RESCHEDULE a appointment', async () => {
      await appointmentRepository.rescheduleAppointment(existingBooking.id, user.id);

      const appointmentState = await appointmentStateRepository.getLatestStateByAppointmentId(existingBooking.id);

      expect(appointmentState.state).toBe(APPOINTMENT_STATE.RESCHEDULED);
    })

    test('RESCHEDULE a appointment', async () => {
      await appointmentRepository.rescheduleAppointment(existingBooking.id, user.id);

      const appointmentState = await appointmentStateRepository.getLatestStateByAppointmentId(existingBooking.id);

      expect(appointmentState.state).toBe(APPOINTMENT_STATE.RESCHEDULED);
    })

  })

  describe('Get Data', () => {

    let coach;
    let user;
    let existingBooking;

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();

      user = await userRepository.insertAsUser(newUserInput2);
      coach = await userRepository.insertAsCoach(newUserInput);
      const appointmentInput = {
        coachId: coach.id,
      }

      existingBooking = await appointmentRepository.bookAppointment(appointmentInput, user.id);
    });

    test('GET all appointment by user', async () => {
      const appointments = await appointmentRepository.findAll({
        userId: user.id,
      });

      expect(appointments.length).toBe(1);
    })

    test('GET all appointment by user', async () => {
      const appointments = await appointmentRepository.findAll({
        coachId: coach.id,
      });

      expect(appointments.length).toBe(1);
    })
  });
});