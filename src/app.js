const express = require("express");

const healthRoutes = require('./routes/health');
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const appointmentRoutes = require('./routes/appointment');

const app = express();

app.use(express.json({ limit: process.env.JSON_LIMIT || "1000kb" }));

app.get('/', (req, res) => {
    res.send('Express Server');
});

app.use('/health', healthRoutes);
app.use('/auth', authRoutes);
app.use('/user', userRoutes);
app.use('/appointment', appointmentRoutes);

module.exports = app