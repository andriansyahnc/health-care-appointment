const httpStatus = require("http-status");
const appointmentRepository = require('../repositories/appointment');
const userRepository = require('../repositories/user');
const {isProposedTimeValid} = require("../utils/dateutil");

const isProposedTimeAppointmentExists = (proposed, userId) => appointmentRepository.findApprovedAppointmentBetweenProposedTime(proposed, userId)

const bookAppointment = async (proposed, user) => {
  const coachData = await userRepository.getCoachDataById(proposed.coachId);
  if (!coachData) {
    return [false, 'Coach not found', httpStatus.NOT_FOUND];
  }
  const isValid = isProposedTimeValid({ schedule: coachData.Schedules }, coachData.tz, proposed, user.tz)
  if (!isValid) {
    return [false, 'Proposed time is not valid', httpStatus.UNPROCESSABLE_ENTITY];
  }
  const count = await isProposedTimeAppointmentExists(proposed, user.id);
  if (count > 0) {
    return [false, 'Another appointment time is set up for this coach', httpStatus.UNPROCESSABLE_ENTITY];
  }
  const newAppointment = await appointmentRepository.bookAppointment(proposed, user.id);
  return [true, newAppointment, httpStatus.OK];
}

module.exports = bookAppointment;