const { ROLE } = require("../../constants/user");
const auth = require("../auth");
const isUser = require('../isUser');

// Mock the required dependencies
jest.mock("../auth");

// Mock the request and response objects
const req = {};
const res = { locals: {} };
const next = jest.fn();

// Define a test user
const testUser = {
  role: ROLE.USER,
};

describe("isUser middleware", () => {
  beforeEach(() => {
    // Clear any previous mock implementation calls
    auth.mockClear();
  });

  it("should call auth function with the request and response objects", () => {
    auth.mockReturnValue(testUser);

    isUser(req, res, next);

    expect(auth).toHaveBeenCalledWith(req);
  });

  it("should set res.locals.user with the user role if the user is a user", () => {
    auth.mockReturnValue(testUser);

    isUser(req, res, next);

    expect(res.locals.user).toEqual(testUser);
  });

  it("should call next if the user is a user", () => {
    auth.mockReturnValue(testUser);

    isUser(req, res, next);

    expect(next).toHaveBeenCalled();
  });

  it("should call next with an error if the user is not a user", () => {
    auth.mockReturnValue(ROLE.COACH);

    isUser(req, res, next);

    expect(next).toHaveBeenCalledWith(new Error('Forbidden'));
  });

  it("should call next with the thrown error if auth throws an error", () => {
    const testError = new Error("Authentication failed");
    auth.mockImplementation(() => {
      throw testError;
    });

    isUser(req, res, next);

    expect(next).toHaveBeenCalledWith(testError);
  });
});
