const newUserInput = {
  email: '4andriansyah@gmail.com',
  name: 'Andriansyah',
  password: 'my-secret-password',
  tz: 'America/Vancouver',
};

const newUserInput2 = {
  email: 'andriansyahnc@gmail.com',
  name: 'M Andrian',
  password: 'password-is-great',
  tz: 'Asia/Jakarta',
};

const newUserResponse = {
  email: newUserInput.email,
  name: newUserInput.name,
  tz: newUserInput.tz,
}

module.exports = {
  newUserInput,
  newUserResponse,
  newUserInput2,
}