const crypto = require('crypto');
const jwt = require("jsonwebtoken");
const config = require("../configs/config");

const getPassword = (password) => {
  const salt = crypto.randomBytes(16).toString('hex');
  const hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`);

  return [hash, salt];
}

const isPasswordValid = (password, salt, hashedPassword) => {
  const hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`);
  return hashedPassword === hash;
}

const signObject = (obj) => jwt.sign(obj, config.key.jwt);
const verifySignedObject = (str) => {
  try {
    return jwt.verify(str, config.key.jwt);
  } catch (e) {
    return null;
  }
}

module.exports = {
  getPassword,
  isPasswordValid,
  signObject,
  verifySignedObject,
}