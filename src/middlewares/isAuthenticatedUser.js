const auth = require("./auth");

const isAuthenticatedUser = (req, res, next) => {
  try {
    const user = auth(req);
    res.locals.user = user;
    return next();
  } catch (e) {
    return next(e);
  }
}

module.exports = isAuthenticatedUser;