const request = require("supertest");
const app = require("../../app");
const {newUserInput} = require("../../../test/data/user");
const bookAppointmentMock = require('../../services/bookAppointment');
const approveAppointmentMock = require('../../services/approveAppointment');
const rejectAppointmentMock = require('../../services/rejectAppointment');
const rescheduleAppointmentMock = require('../../services/rescheduleAppointment');
const getAppointmentsMock = require('../../services/getAppointments');
const {signObject} = require("../../utils/utils");
const {ROLE} = require("../../constants/user");

jest.mock("../../services/bookAppointment");
jest.mock("../../services/approveAppointment");
jest.mock("../../services/rejectAppointment");
jest.mock("../../services/rescheduleAppointment");
jest.mock("../../services/getAppointments");

describe("Book Appointment", () => {
  it("should return 200", async () => {
    const mockRequestBody = {
      startTime: "1 January 2023 00:30",
      endTime: "1 January 2023 01:30",
      coachId: 1,
    };

    const mockUser = {
      id: 2,
      email: newUserInput.email,
      role: ROLE.USER,
      tz: newUserInput.tz,
    };

    const token = signObject(mockUser);

    const response = await request(app)
      .post("/appointment")
      .set("Authorization", `Bearer ${token}`)
      .send(mockRequestBody);

    expect(bookAppointmentMock).toHaveBeenCalledWith(
      mockRequestBody,
      {
        ...mockUser,
        iat: expect.anything(),
      }
    );

    expect(response.body.success).toBe(true);
    expect(response.body.data.newAppointment.coachId).toBe(1);
  });
});

describe("Approve appointment", () => {
  it("should return 200", async () => {
    const mockUser = {
      id: 2,
      email: newUserInput.email,
      role: ROLE.COACH,
      tz: newUserInput.tz,
    };

    const token = signObject(mockUser);

    const response = await request(app)
      .post("/appointment/1/approve")
      .set("Authorization", `Bearer ${token}`);

    expect(approveAppointmentMock).toHaveBeenCalledWith("1", { ...mockUser, iat: expect.anything() });

    expect(response.body.success).toBe(true);
    expect(response.body.message).toBe('OK');
  });
});

describe("Reject appointment", () => {
  it("should return 200", async () => {
    const mockUser = {
      id: 2,
      email: newUserInput.email,
      role: ROLE.USER,
      tz: newUserInput.tz,
    };

    const token = signObject(mockUser);

    const response = await request(app)
      .post("/appointment/1/reject")
      .set("Authorization", `Bearer ${token}`);

    expect(rejectAppointmentMock).toHaveBeenCalledWith("1", { ...mockUser, iat: expect.anything() });

    expect(response.body.success).toBe(true);
    expect(response.body.message).toBe('OK');
  });
});

describe("Reschedule appointment", () => {
  it("should return 200", async () => {
    const mockUser = {
      id: 2,
      email: newUserInput.email,
      role: ROLE.COACH,
      tz: newUserInput.tz,
    };

    const token = signObject(mockUser);

    const payload = {
      startTime: '2023-01-01 00:00',
      endTIme: '2023-01-02 00:00'
    };

    const response = await request(app)
      .post("/appointment/1/reschedule")
      .set("Authorization", `Bearer ${token}`)
      .send(payload);

    expect(rescheduleAppointmentMock).toHaveBeenCalledWith("1", { ...mockUser, iat: expect.anything() }, payload.startTime, payload.endTime);

    expect(response.body.success).toBe(true);
    expect(response.body.message).toBe('OK');
  });
});

describe("Get appointments", () => {
  it("should return 200", async () => {
    const mockUser = {
      id: 2,
      email: newUserInput.email,
      role: ROLE.COACH,
      tz: newUserInput.tz,
    };

    const token = signObject(mockUser);

    const response = await request(app)
      .get("/appointment")
      .set("Authorization", `Bearer ${token}`);

    expect(getAppointmentsMock).toHaveBeenCalledWith({ ...mockUser, iat: expect.anything() });

    expect(response.body.success).toBe(true);
    expect(response.body.message).toBe('OK');
  });
});
