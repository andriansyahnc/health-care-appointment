const httpStatus = require("http-status");
const rescheduleAppointment = require("../rescheduleAppointment");
const appointmentRepository = require('../../repositories/appointment');

jest.mock('../../repositories/appointment');

describe('Reschedule Appointment', () => {

  test('I successfully reschedule an appointment', async () => {

    const mockAppointmentId = 1;
    const mockUser = {
      id: 2,
    };

    const mockAppointment = {
      id: mockAppointmentId,
      coachId: mockUser.id,
    };

    appointmentRepository.findById.mockReturnValue(mockAppointment);

    const result = await rescheduleAppointment(mockAppointmentId, mockUser);
    expect(appointmentRepository.findById).toHaveBeenCalledWith(mockAppointmentId);
    expect(result).toEqual([true, "OK", httpStatus.OK]);
  })
});