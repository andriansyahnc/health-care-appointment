const userRepository = require('../user');
const {newUserInput, newUserInput2} = require("../../../test/data/user");
const globalDatabase = require("../../../test/function/makeGlobalDatabase");
const db = require("../../models");
const {schedule1} = require("../../../test/data/coachSchedule");
const scheduleRepository = require("../schedule");
const {ROLE} = require("../../constants/user");

describe('User Repositories', () => {

  afterAll(async () => {
    await globalDatabase.closeGlobalDatabase();
    // eslint-disable-next-line no-promise-executor-return
    await new Promise((resolve) => setTimeout(() => resolve(), 1000));
  })

  describe('Register New User', () => {

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();
    });

    test('Insert user as coaches', async () => {

      const newCoachInput = {
        ...newUserInput,
        role: ROLE.COACH,
      };

      const newCoach = await userRepository.insertAsCoach(newUserInput);

      expect(newCoach.password).not.toEqual(newCoachInput.password);

      delete newCoachInput.password;

      expect(newCoach).toMatchObject(newCoachInput)
    })

    test('Insert user as user', async () => {

      const newDataUserInput = {
        ...newUserInput,
        role: 'user',
      };

      const newUser = await userRepository.insertAsUser(newUserInput);

      expect(newUser.password).not.toEqual(newDataUserInput.password);

      delete newDataUserInput.password;

      expect(newUser).toMatchObject(newDataUserInput)
    });
  });

  describe('Login', () => {

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();

      await userRepository.insertAsUser(newUserInput2);
      await userRepository.insertAsCoach(newUserInput);
    });

    test.each([
      ['coach', newUserInput.email],
      ['user', newUserInput2.email],
    ])('Login user - %s', async (label, email) => {
      const userData = await userRepository.findByEmail(email);
      expect(userData.email).toBe(email);
    })
  })

  describe('getCoachDataById', () => {

    let coach;

    beforeEach(async () => {
      await globalDatabase.syncGlobalDatabase();

      await userRepository.insertAsUser(newUserInput2);
      coach = await userRepository.insertAsCoach(newUserInput);

      const t = await db.sequelize.transaction();
      const scheduleInputs = schedule1.map(schedule => ({
        ...schedule,
        userId: coach.id
      }))
      await scheduleRepository.upsertMany(scheduleInputs, t);
      await t.commit();
    });

    test('succeed getCoachDataById', async () => {
      const coachData = await userRepository.getCoachDataById(coach.id);
      expect(coachData.Schedules.length).toBe(schedule1.length);
      expect(coachData.role).toBe(ROLE.COACH);
    })
  })
})