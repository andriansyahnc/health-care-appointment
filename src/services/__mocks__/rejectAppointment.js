const httpStatus = require("http-status");

const rejectAppointmentMock = jest.fn().mockReturnValue([
  true, 'OK', httpStatus.OK]
);

module.exports = rejectAppointmentMock;
