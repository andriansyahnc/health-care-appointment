
const {
  Model
} = require('sequelize');
const { APPOINTMENT_STATE: { PENDING, APPROVED, CANCELLED, REJECTED, RESCHEDULED, TERMINATED} } = require('../constants/appointment');

module.exports = (sequelize, DataTypes) => {
  class AppointmentState extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Appointment);
    }
  }
  AppointmentState.init({
    appointmentId: DataTypes.INTEGER,
    state: {
      type: DataTypes.ENUM,
      values: [PENDING, APPROVED, RESCHEDULED, REJECTED, CANCELLED, TERMINATED],
    },
    detail: DataTypes.STRING,
    startTime: DataTypes.DATE,
    endTime: DataTypes.DATE,
    createdById: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'AppointmentState',
    underscored: true,
  });
  return AppointmentState;
};