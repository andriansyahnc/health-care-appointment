const httpStatus = require("http-status");
const bookAppointment = require("../bookAppointment");
const {APPOINTMENT_STATE} = require("../../constants/appointment");
const appointmentRepository = require('../../repositories/appointment');
const userRepository = require('../../repositories/user');
const {ROLE} = require("../../constants/user");
const utils = require('../../utils/dateutil');

jest.mock('../../repositories/appointment');
jest.mock('../../repositories/user');
jest.mock('../../utils/dateutil');

describe('Appointment', () => {

  describe('Booking Appointment', () => {

    test('I successfully booked an appointment', async () => {

      const mockUser = {
        id: 2,
        role: ROLE.USER,
      };

      const mockCoach = {
        id: 3,
        role: ROLE.COACH,
      }

      const mockAppointment = {
        coachId: mockCoach.id,
        startTime: "31 January 2023 10:00",
        endTime: "31 January 2023 10:30",
      }

      userRepository.getCoachDataById.mockReturnValue(mockCoach);
      utils.isProposedTimeValid.mockReturnValue(true);
      appointmentRepository.bookAppointment.mockReturnValue({ ...mockAppointment, userId: mockUser.id })

      const [success, newBooking, status] = await bookAppointment(mockAppointment, mockUser);

      expect(success).toBe(true);
      expect(status).toBe(httpStatus.OK);
      expect(appointmentRepository.bookAppointment).toBeCalledWith(mockAppointment, mockUser.id)
    })

  })
});