const jwt = require('jsonwebtoken');
const loginUser = require('../loginUser');
const {newUserInput, newUserInput2} = require("../../../test/data/user");
const config = require('../../configs/config');
const globalDatabase = require("../../../test/function/makeGlobalDatabase");
const userRepository = require("../../repositories/user");

describe('Login User', () => {

  beforeEach(async () => {
    await globalDatabase.syncGlobalDatabase();

    await userRepository.insertAsUser(newUserInput2);
    await userRepository.insertAsCoach(newUserInput);
  });

  afterAll(async () => {
    await globalDatabase.closeGlobalDatabase();
    // eslint-disable-next-line no-promise-executor-return
    await new Promise((resolve) => setTimeout(() => resolve(), 1000));
  });

  test.each([
    ['coach', newUserInput.email, newUserInput.password],
    ['user', newUserInput2.email, newUserInput2.password],
  ])('successfully login - %s', async (label, email, password) => {
    const input = {
      email,
      password,
    }

    const [isSucceed, token] = await loginUser(input);
    const verifiedData = jwt.verify(token, config.key.jwt);

    expect(verifiedData.email).toBe(email);
    expect(verifiedData.role).toBe(label);
    expect(isSucceed).toBe(true);
  });

  test('user not found', async () => {
    const input = {
      email: 'babang.hensem@gmail.com',
      password: 'password',
    }
    const [isSucceed, message] = await loginUser(input);

    expect(isSucceed).toBe(false);
    expect(message).toBe('User not found');
  });

  test('user not authorized', async () => {
    const input = {
      email: newUserInput.email,
      password: 'password',
    }
    const [isSucceed, message] = await loginUser(input);

    expect(isSucceed).toBe(false);
    expect(message).toBe('User not authorized');
  });
})