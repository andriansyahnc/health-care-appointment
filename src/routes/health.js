const router = require('express').Router();
const controller = require('../controller/health');

router.get('/', controller.getHealth);

module.exports = router;