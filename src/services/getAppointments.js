const httpStatus = require("http-status");
const appointmentRepository = require('../repositories/appointment');
const {ROLE} = require("../constants/user");

const getAppointment = async (user, state) => {
  const where = {};
  if (user.role === ROLE.COACH) {
    where.coachId = user.id;
  } else {
    where.userId = user.id;
  }

  if (state) {
    where.state = state;
  }

  const appointments = await appointmentRepository.findAll(where);
  if (!appointments || appointments.length === 0) {
    return [false, 'No Appointment found', httpStatus.NOT_FOUND];
  }

  return [true, appointments, httpStatus.OK];
}

module.exports = getAppointment;