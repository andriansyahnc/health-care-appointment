const router = require('express').Router();
const controller = require('../controller/appointment');
const isUser = require('../middlewares/isUser');
const isCoach = require('../middlewares/isCoach');
const isAuthenticatedUser = require('../middlewares/isAuthenticatedUser');

router.get('/', isAuthenticatedUser, controller.getAppointments);
router.post('/', isUser, controller.bookAppointment);
router.post('/:id/approve', isCoach, controller.approveAppointment);
router.post('/:id/reject', isAuthenticatedUser, controller.rejectAppointment);
router.post('/:id/reschedule', isCoach, controller.rescheduleAppointment);

module.exports = router;