const httpStatus = require("http-status");
const updateScheduleService = require("../services/updateSchedule")

const updateSchedule = async (req, res, next) => {
  try {
    const userId = res.locals.user.id;
    const inputs = req.body.data;
    const scheduleInput = inputs.map(input => ({
      ...input,
      userId
    }))
    const data = await updateScheduleService(scheduleInput);
    res.status(httpStatus.OK).send({
      success: true,
      message: 'OK',
      data,
    })
  } catch (e) {
    next(e);
  }
}

module.exports = {
  updateSchedule,
}