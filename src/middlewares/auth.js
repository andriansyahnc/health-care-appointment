const {verifySignedObject} = require("../utils/utils");

const auth = (req) => {
  const authorizationToken = req.header('authorization')
  const bearerTokens = authorizationToken.split(' ');

  if (bearerTokens.length !== 2) {
    const error = Error('Bearer token not valid');
    throw error;
  }

  const bearerToken = bearerTokens[1];
  const verifiedObject = verifySignedObject(bearerToken);
  if (!verifiedObject) {
    const error = Error('Unauthorized');
    throw error;
  }

  return verifiedObject;
}

module.exports = auth;