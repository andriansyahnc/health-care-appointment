const APPOINTMENT_STATE = {
  PENDING: 'pending',
  APPROVED: 'approved',
  RESCHEDULED: 'rescheduled',
  REJECTED: 'rejected',
  CANCELLED: 'cancelled',
  TERMINATED: 'terminated'
};

module.exports = {
  APPOINTMENT_STATE,
}