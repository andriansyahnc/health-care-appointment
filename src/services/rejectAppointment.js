const httpStatus = require("http-status");
const appointmentRepository = require('../repositories/appointment');
const {ROLE} = require("../constants/user");

const rejectAppointment = async (appointmentId, user) => {
  const appointment = await appointmentRepository.findById(appointmentId);
  if (!appointment) {
    return [false, 'Appointment not found', httpStatus.NOT_FOUND];
  }

  if (user.role === ROLE.COACH && appointment.coachId !== user.id) {
    return [false, 'Forbidden', httpStatus.FORBIDDEN];
  }
  if (user.role === ROLE.USER && appointment.userId !== user.id) {
    return [false, 'Forbidden', httpStatus.FORBIDDEN];
  }
  await appointmentRepository.rejectAppointment(appointmentId, user.id);
  return [true, 'OK', httpStatus.OK];
}

module.exports = rejectAppointment;